class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.string :identification_card, index: true, default: ""
      t.string :email, index: true, default: ""
      t.string :name, index: true, default: ""
      t.string :phone, default: ""
      t.references :movie, foreign_key: true
      t.references :schedule, foreign_key: true

      t.timestamps
    end
  end
end
