class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.date :schedule, index: true
      t.references :movie, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
