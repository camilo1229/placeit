Rails.application.routes.draw do
  resources :movies, except: [:destroy] do
    member do
      post 'enable'
      post 'disable'
    end
    collection do
      get 'search'
    end
  end
  resources :reservations, only:[:create, :index, :show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
