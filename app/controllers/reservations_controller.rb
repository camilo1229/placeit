class ReservationsController < ApplicationController
  before_action :set_movie, only: [:create]
  before_action :reservation_available?, only: [:create]

  def index
    @reservations = Reservation.includes(:movie, :schedule).collect(&:details)
    render json: @reservations
  end
  
  def create
    reservation = Reservation.new(reservation_params)
    if reservation.save
      render json: {message: "Reservation success", reservation: reservation.details}, status: :created
    else
      render json: { message: reservation.errors.full_messages.to_sentence}, status: :unprocessable_entity
    end
  end

  private
    def set_movie
      if @movie = Movie.find_by(id: params[:reservation][:movie_id], status: "active")
        puts params[:reservation][:schedule_id]
        puts "despues de ---\n"*10
        if (!params[:reservation].has_key?(:schedule_id) || params[:reservation][:schedule_id].blank?) || !@movie.schedules.active.find_by(id: params[:reservation][:schedule_id])
          return render json: {message: "El horario seleccionado no corresponde a la pelicula o esta inactivo"}, status: :unprocessable_entity
        end
      else
        return render json: {message: "La pelicula no exite o esta inactiva"}, status: :bad_request
      end
    end

    def reservation_params
      params.require(:reservation).permit(:identification_card, :name, :phone, :email, :movie_id, :schedule_id)
    end

    def reservation_available?
      if Reservation.where(movie_id: @movie.id, schedule_id: params[:reservation][:schedule_id]).count >= 10
        return render json: {message: "No hay sillas disponibles para esta función"}, status: :unprocessable_entity
      end
    end
end
