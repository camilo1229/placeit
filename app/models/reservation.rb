class Reservation < ApplicationRecord
  belongs_to :movie
  belongs_to :schedule

  def details
    {
      id: id, name: name, phone: phone, email: email, identification_card: identification_card,
      movie: {
        id: movie.id, name: movie.name, image: movie.image, schedule: schedule.schedule
      }
    }
  end
end
